import java.util.InputMismatchException;
import java.util.Scanner;

public class DragonCaveProject {
    public static void main(String[] args) {

        Scanner getInput = new Scanner(System.in);

        System.out.println("\n" + "You are in a land full of dragons. In front of you,\n" +
                "\n" +
                "you see two caves. In one cave, the dragon is friendly\n" +
                "\n" +
                "and will share his treasure with you. The other dragon\n" +
                "\n" +
                "is greedy and hungry and will eat you on sight.\n" +
                "\n" +
                "Which cave will you go into? (1 or 2)\n" + "\n");
        int input;

        while (true) {
            //System.out.println("Which cave will you go into? (1 or 2)\n" + "\n");
            try {
                input = getInput.nextInt();
                System.out.println();
                System.out.println();
                System.out.println("Enter the correct number (1 or 2)\n" + "\n");
                if (input == 1 || input == 2) {
                    break;
                }
            } catch (Exception e) {
                getInput.next();
                System.out.println();
                System.out.println();
                System.out.println("Invalid input! Enter a number (1 or 2)\n" + "\n");
            }
        }

        //int input = getInput.nextInt();
        System.out.println("\n");


        if (input == 1) {
            System.out.println("You approach the cave...\n" +
                    "\n" +
                    "It is dark and spooky...\n" +
                    "\n" +
                    "A large dragon jumps out in front of you! He opens his jaws and...\n" +
                    "\n" +
                    "Gobbles you down in one bite!\n");
        } else {
            System.out.println("You approach the cave...\n" +
                    "\n" +
                    "It is bright and beautiful...\n" +
                    "\n" +
                    "A small dragon jumps out in front of you! He smiles and...\n" +
                    "\n" +
                    "Says I LOVE YOU!\n");
        }
    }
}
